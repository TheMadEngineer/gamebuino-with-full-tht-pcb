# GameBuino with full THT PCB
=============================


## Description
A DIY gamebuino with a full THT PCB that keeps a small form factor.

## Authors and acknowledgment
Schematics from the BBGamebuino project,
PCB by me,
fimrmware from the original Gamebuino.
More info on my website:
https://dreamprojects5.wordpress.com/2023/04/06/diy-gamebuino-with-a-nice-pcb/

## License
CC BY 4.0

## Project status
This project is about complete but it is still missing a proper case.